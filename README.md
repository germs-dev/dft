---
title: The Plotting Parlour
author: Dean Turpin
---

## Discrete Fourier Transforms in C++23

The aim of this project is to explore writing an FFT (really a DFT) from scratch and an opportunity to flex the latest C++. The aspiration is to keep up with realtime audio without depending on third-party libraries or firmware. See [the pipeline](https://gitlab.com/germs-dev/dft/-/blob/main/.gitlab-ci.yml) for this repo.

[![DFT result rendered by gnuplot](glass2.csv.png)](glass2.csv.png)

## Profiling

The output of `gprof` is pretty unfriendly to parse for a person, but piping it through `gprof2dot` is much clearer.

[![Profile hot spots](prof.svg)](prof.svg)

## Unit testing

Just for fun -- and to remove the dependency on gtest -- I've done all unit testing using static assertions. But I am still using Google Benchmark.

## Notable optimistations

- `g++` over `clang++`
- `-Ofast`
- `float` instead of `double` (also lets you fit a larger twiddle matrix in memory)
- Keep your laptop plugged in :)

## Running on a reasonably-powered VM

Performance of a 16-core Google Cloud VM with 62GB of RAM:

- 16 core
- 20 files
- 1310720 samples
- 8 GiB twiddle matrix
- 13.163861 seconds analysis
- 99569.57 Hz sample processing
- 65536 bin count
- 1.5193111 x speed up

## The code

