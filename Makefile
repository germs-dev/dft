all:
	cmake -B build -S .
	cmake --build build --parallel --target run

entr:
	ls *.cxx CMakeLists.txt | entr -cr make

clean:
	$(RM) -r build/
